<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Core extends CI_Controller
{
    protected $layout;

    public function __construct()
    {
        parent::__construct();

        $this->layout = 'layouts/main_layout';

        date_default_timezone_set("Asia/Kuala_Lumpur");
    }
} 