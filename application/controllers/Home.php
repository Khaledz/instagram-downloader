<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Core
{
	private function initInstagram()
	{
		$api = array(
			'apiKey' => '86b27ed051824f888b04a0c30c542ee0',
			'apiSecret' => '46097aeb022c4e36a4330b8a35a0322e',
			'apiCallback' => 'http://localhost/instagram/'
		);

	    $this->load->library('instagram', $api);
	}

	public function index()
	{	
		// ini_set('max_execution_time', 1);

		$data = array(
			'title'  => 'Home',
			'view'   => 'index',
			'csrf_name' => $this->security->get_csrf_token_name(),
			'csrf_hash' => $this->security->get_csrf_hash()
		);

		if($this->input->post())
		{		
			$this->form_validation->set_rules('username', 'user name', 'required|strip_tags|trim');
			$this->form_validation->set_rules('type', 'type', 'required|strip_tags|trim');

			if($this->security->xss_clean($this->input->post('username'), TRUE) === FALSE)
			{
				echo $data['errors'][] = '0A threat has been detected, if you feel this is wrong message please contact us.';
        		return;
			}

			$username = $this->security->sanitize_filename($this->input->post('username'));

			if(!$this->form_validation->run())
        	{
        		echo $data['errors'][] = '0'.validation_errors();
        		return;
        	}

        	// start a new call.
			$this->initInstagram();

        	if($this->instagram->searchUser($username)->data == NULL)
        	{
        		echo $data['errors'][] = '0This user is not found.';
        		return;
        	}

        	//get the user information.
			$data['user'] = $this->instagram->searchUser($username)->data[0];

			// get the media.
			$result = $this->instagram->getUserMedia((int)$data['user']->id);

        	// if user is privat
        	if($result->meta->code == 400)
			{
				echo $data['errors'][] = '0This user is private.';
				return;
			}

			if($this->input->post('type') == 'all')
			{
				$zipFileName = $data['user']->username . '.zip';

				$dir = substr('assets/download/' .$zipFileName, 0, -4);

				//create a new folder to hold temp files.
				if(!file_exists($dir))
				{
					//create new folder.
					mkdir($dir);

					//create new subfolder video.
				    mkdir($dir . '/Videos');

				    //create new subfolder images.
				    mkdir($dir . '/Images');	
				}

				//used for naming files.
				$i = 1;

				$count = $this->instagram->getUser((int)$data['user']->id)->data->counts->media; 

				do 
				{
				    foreach ($result->data as $media)
				    {
				    	$percent = intval($i/$count * 100);
				        if ($media->type === 'video')
				        {		
				        	// download videos in a folder.
				        	@copy($media->videos->low_resolution->url, $dir . '/Videos/' . $i++ . '.mp4');
				        	//$data['videos'][] = $media->videos->low_resolution->url;
				        	//system('wget -q '.$media->videos->low_resolution->url.' -O '. $dir . '/Videos/' . $i++.'.mp4');
				        }
				        else
				        {
				        	// download images in a folder.
				        	@copy($media->images->standard_resolution->url, $dir . '/Images/' . $i++ . '.jpg');
				        	//$data['images'][] = $media->images->standard_resolution->url;
				        	//system('wget -q '.$media->images->standard_resolution->url.' -O '. $dir . '/Images/' . $i++.'.jpg');
				        }
				    }

				    //move to the next 20 results.
				    $result = $this->instagram->pagination($result);

				} while ($result);

				if(!$this->zipDir($dir))
				{
					echo $data['errors'][] = '0Cannot zip the result.';
					return;
				}
				else
				{
					if($this->cleanDir($dir))
					{
						echo $data['success'][] = '<a href="'.$dir . '.zip">click here to download the zip file</a>';
						return;
					}
				}			

			}
			else if($this->input->post('type') == 'partial')
			{
				do 
				{
				    foreach ($result->data as $media)
				    {
				        if ($media->type === 'video')
				        {
				            $data['videos'][] = $media->videos->standard_resolution->url;
				        }
				        else
				        {
				            $data['images'][] = $media->images->standard_resolution->url;
				        }
				    }

				    $result = $this->instagram->pagination($result);

				} while ($result);
			}
		}

		$this->load->view($this->layout, $data);
	}

	private function cleanDir($dir)
	{
		if(!file_exists($dir))
		{
			return false;
		}

		$files = array_diff(scandir($dir), array('.', '..'));
		
        foreach ($files as $folder)
        {
        	$files = array_diff(scandir($dir . '/' . $folder), array('.', '..'));

        	for($i = 2; $i < count($files) + 2; $i++)
        	{
        		unlink($dir. '/' . $folder . '/' . $files[$i]);
        	}

        	rmdir($dir. '/' . $folder);
        }

        return rmdir($dir);
	}

	private function zipDir($dir)
	{
		$zip = new ZipArchive();

		$zip->open($dir. '.zip', ZipArchive::CREATE | ZIPARCHIVE::OVERWRITE);
		

		//create new subfolder videos.
		$zip->addEmptyDir('Videos');

		//create new subfolder images.
		$zip->addEmptyDir('Images');

		//get the path of folder.
		$rootPath = realpath($dir);

		$files = new RecursiveIteratorIterator(
			     new RecursiveDirectoryIterator($rootPath),
			     RecursiveIteratorIterator::LEAVES_ONLY
			);

		foreach ($files as $name => $file)
		{
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir())
		    {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		    }
		}

		$zip->close();

		return true;
	}
}
