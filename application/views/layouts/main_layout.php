<!DOCTYPE html>
<html lang="en">
<head>  
  <meta charset="utf-8" />
  <title><?php echo isset($title) ?  $title . ' - Instagram Downloader' : 'Instagram Downloader'; ?></title>
  <meta name="description" content="Angularjs, Html5, Music, Landing, 4 in 1 ui kits package" />
  <meta name="keywords" content="AngularJS, angular, bootstrap, admin, dashboard, panel, app, charts, components,flat, responsive, layout, kit, ui, route, web, app, widgets" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" type="text/css" />
  <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>


</head>
<body>
	
  <!-- header -->
  <header id="header" class="navbar navbar-fixed-top bg-white-only padder-v"  data-spy="affix" data-offset-top="1">
    <div class="container">
      <div class="navbar-header">
        <button class="btn btn-link visible-xs pull-right m-r" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
        <a href="<?php echo base_url(); ?>" class="navbar-brand m-r-lg"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="m-r-sm hide"><span class="h3 font-bold">Instgrami</span></a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav font-bold">
          <li>
            <a href="#what" data-ride="scroll">How it works?</a>
          </li>
          <li>
            <a href="#features" data-ride="scroll">Features</a>
          </li>
          <li>
            <a href="#why" data-ride="scroll">About Us</a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li>
            <div class="m-t-sm">
              <p class="btn-md">Demo Version</p> 
            </div>
          </li>
        </ul>     
      </div>
    </div>
  </header>
  <!-- / header -->

  <div id="content">
    
    <br>
    <br>
    <div class="bg-light">
      <div class="container">
        <div class="m-t-xxl m-b-xl text-center wrapper">
          <h2 class="text-black font-bold">Welcome to Instgrami, It's free to use!</h2>
          <p class="text-muted h4">Type a username to get start</p>
        </div>
        <div class="row m-t-xl m-b-xl text-center">
          <div class="col-sm-12" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            
            <?php

              if(isset($view) && isset($data))
              {
                $this->load->view($view, $data);
              }
              elseif(isset($view) && !isset($data))
              {
                $this->load->view($view);
              }
              else
              {
                echo 'Cannot load content';
              }

            ?>

          </div>
        </div>

      </div>
    </div>
    
  <footer>  
    <div class="bg-light dk">
      <div class="container">
        <div class="row padder-v s-t">
          <div class="col-xs-12 text-center">
            Instagram Downloader <?php echo date('Y'); ?>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->
  <script src="<?php echo base_url(); ?>assets/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/jquery/jquery_appear/jquery.appear.js"></script>
</body>
</html>
