<form class="form-horizontal" method="post">
    <div class="form-group">
      <div class="col-sm-12">
        <input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" class="form-control rounded input-lg" placeholder="Type username here ...">                        
      	<input type="hidden" name="<?=$csrf_name;?>" value="<?=$csrf_hash;?>"
      </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
    	<div class="form-group">
	    	<div class="col-lg-6">
		    	<div class="radio">
		          <label class="i-checks">
		            <input type="radio" name="type" value="all" class="typeVal" checked>
		            <i></i>
		            Downlad All Videos & Images
		          </label>
		    </div>
		</div>
		<div class="form-group">
		    <div class="col-lg-6">
		          <div class="radio">
		              <label class="i-checks">
		                <input type="radio" name="type" value="partial">
		                <i></i>
		                Preview and let me choose 
		              </label>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
    </div>
    <button type="submit" class="btn btn-dark" id="download">Start Download</button>

</form>
<hr>

<center>
	<div id="loading" style="display:none">
	  <img src="<?php echo base_url(); ?>/assets/img/loading.gif" width="320"/><p><h4> Please wait while we generate a zip file ...</h4></p>
	</div>
</center>

<div class="error-message" style="display:none">
	<hr>
	<div class="panel panel-danger">
	<div class="panel-heading">
		Error
	</div>
	<div class="panel-body">
		<div class="error"></div>
	</div>	
	</div>
</div>

<div class="success-message" style="display:none">
	<hr>
	<div class="panel panel-success">
	<div class="panel-heading">
		Done
	</div>
	<div class="panel-body">
		<div class="success"></div>
	</div>	
	</div>
</div>

<div class="info-message" style="display:none">
	<hr>
	<div class="panel panel-info">
	<div class="panel-heading">
		Almost Done
	</div>
	<div class="panel-body">
		<div class="info"></div>
	</div>	
	</div>
</div>

<?php if(isset($user)): ?>
<br>
<hr>
	<img src="<?php echo $user->profile_picture; ?>" class="img-circle">
	<h3><?php echo $user->full_name; ?></h3>

<?php if(isset($success)): ?>
	<hr>
	<div class="panel panel-success">
	<div class="panel-heading">
		Done!
	</div>
	<div class="panel-body">

		<p><h3><?php echo $success; ?></h3></p>
					
	</div>
	</div>

<?php else: ?>

<hr>
<div class="panel panel-default">
	<div class="panel-heading">
		Videos
	</div>
	<div class="panel-body">

	<?php if(isset($videos)): ?>
		<?php foreach($videos as $video): ?>
		<div class="col-lg-4">
			
			<video width="320" controls>
			  	<source src="<?php echo $video; ?>" type="video/mp4">
				Your browser does not support the video tag.
			</video>

		</div>

		<hr>
		<?php endforeach; ?>

	<?php else: ?>
		
		<p><h3>There is no videos for this user.</h3></p>

	<?php endif; ?>  

	</div>
</div>

<hr>
<div class="panel panel-default">
	<div class="panel-heading">
		Images
	</div>
	<div class="panel-body">
	  
	<?php if(isset($images)): ?>
		<?php foreach($images as $image): ?>

			<img src="<?php echo $image; ?>">

		<?php endforeach; ?>

	<?php else: ?>
		
		<p><h3>There is no images for this user.</h3></p>

	<?php endif; ?>
  
	</div>
</div>

	


<?php endif; ?>	

<?php endif; ?>	

<script type="text/javascript">
	
$(function(){

  $('#loading').hide();
  $('.success-message').hide();
  $('.error-message').hide();

   $('input[name=type]').change(function()
     {
	    // change the page per this logic
	    switch ($('input[name=type]:checked').val()) {
	         case 'all':
	             $('#button').text('Start Download');
	             $('#button').attr('id', 'download');
	             break;
	         case 'partial':
	             $('#button').text('Perview This Account');
	             $('#button').attr('id', 'partial'); 
	             break;
	         default:
	             $('#button').text('Error');
	     }
	 });

  $("#download").click(function(e){

   	$("#download").attr("disabled", true);	
   	$('.success-message').hide();
  	$('.error-message').hide();
  	$('.info-message').hide();
    $('#loading').show(300);

    	e.preventDefault();
    	var username = $('#username').val();
    	
    	$.ajax({

		url: '<?php echo base_url();?>',

		data:{username:username, type:$(".typeVal:checked").val()},

		type: 'POST',

		timeout: 0,

		success: function(data){

			if(data.charAt(0) == '0')
			{
				var test = data;
				$('.error-message').show();
				$('.error').html('<p><h4>'+data.substring(1)+'</p></h4>');
				$('#loading').hide();
				$("#download").attr("disabled", false);	
			}
			else
			{
				$('.success-message').show();
				$('.success').html('<p><h4>'+data+'</p></h4>');
				$('#loading').hide();
				$("#download").attr("disabled", false);	
			}

		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#loading').hide();
        	$('.info-message').show();
			$('.info').html('<p><h4>We need some more time while we proccess your zip file, here is your download link <br><br><a href="assets/download/'+username+'.zip"><?php echo base_url(); ?>assets/download/'+username+'.zip</a></p></h4>');
  	  	}

	});

    });
});

</script>